# xshbar

### Deprecated

xshbar is deprecated. It is not a good bar, it is slow, annoying to configure, and just generally poorly written.
I suggest that you use [dwmblocks](https://github.com/UtkarshVerma/dwmblocks-async) instead.

#### Installation
- Install git
- git clone https://codeberg.org/speedie/xshbar
- cd xshbar
- make install
- See Usage.

#### Usage
xshbar must be used with a window manager that supports xsetroot and xsetroot must be installed. First install xsetroot and then make sure your window manager supports xsetroot.

Once you're sure it does, add ~/.config/xshbar/xshbar & to ~/.xinitrc

Now follow Basic Editing

#### Basic Editing
In order to use xshbar, it needs to have something to print. With xshbar this is done using plugins. A few come bundled with xshbar but not all.

##### cd ~/.config/xshbar/plugins 

and then you will be able to see all available plugins. Let's say we want date.plugin.

##### cp date.plugin ../plugins.use

You can now vim into that file and see all of its variables. We can mention these in the file we're about to edit.

##### cd ~/.config/xshbar && vim prn

prn is the file which contains what we will be printing.

##### vim ~/.config/xshbar/xshbar

Add your plugins to XSHBAR_IMPORT() function.

When you're done, save the file, and kill your X session and startx. Enjoy!
  
#### Credits

- speedie
- Possible contributors.

#### License

xshbar is licensed under the MIT license. The GitHub repository contains a LICENSE file which you can read if you're not sure what terms apply to your usage of this software.

#### NOTE
Please be very careful and look through plugins before adding them to plugins.use. Some plugins may be malicious and delete or steal user data or break your system.